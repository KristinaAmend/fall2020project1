package movies.importer;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Kausar Mussa
 * This class ImdbImporter extends Processor. The overridden process method 
 * will take as input an imdb input file that has multiple movie with multiple values
 * The process method will return an arrayList where each element will keep only
 * the title, year, runtime and source of the movie.
 *
 */

public class ImdbImporter extends Processor 
{
	//constructor
	public ImdbImporter(String sourceDir, String outputDir, boolean header) 
	{
		super(sourceDir, outputDir, true);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> imdbinput) {
		int titleColumn= 1;
		int yearColumn= 3;
		int runtimeColumn= 6;
		int numOfColumns = 22;

		ArrayList <String> moviesImdb = new ArrayList <String>();

		String temp = "";
		String [] movie;

		for(int i=0; i< imdbinput.size(); i++) {
			temp= imdbinput.get(i);
			movie= temp.split("\\t",-1);
			if (movie.length == numOfColumns) //if a movie has more or less columns than 22 we do not want that movie in the returned array list 
			{	//to have each movie in the same format, we store the value in a Movie object than call toString on the movie. To keep it consistent and easier
				Movie toadd = new Movie (movie[titleColumn],movie[runtimeColumn],movie[yearColumn], "imdb");
				moviesImdb.add(toadd.toString());
			}
			else 
			{
				break;
			}
		}
		return moviesImdb;
	}

	
}
