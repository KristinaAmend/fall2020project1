package movies.importer;
import java.io.IOException;

/**
 * @author Kausar Mussa
 * @author Kristina Amend 
 * This is the main part of this application. Create appropriate Processors 
 * and put in an [] and then call the processAll method 
 * to call the execute() on each element of the []. Will load feeds of both files, 
 * normalize them, validate them, and finally dedupe them to produce a single file.
 */

public class ImportPipeline {

	public static void main(String[]args) throws IOException {

		// file paths for kaggle, imdb, merged output
		String kaggleSrc = "C:\\MovieFiles\\kaggle";
		String kaggleDest = "C:\\MovieFiles\\kaggleOut";
		String mergedOutput = "C:\\MovieFiles\\mergedOutput";
		String imdbSrc = "C:\\MovieFiles\\imdb";
		String imdbDest = "C:\\MovieFiles\\imdbOut";
		
		
		KaggleImporter processKaggle = new KaggleImporter(kaggleSrc, kaggleDest);
		Normalizer normalize = new Normalizer(kaggleDest, mergedOutput);
		
		ImdbImporter processImdb = new ImdbImporter(imdbSrc, imdbDest, true);
		Validator validate = new Validator(imdbDest, mergedOutput, false);
		
		Deduper dedupe = new Deduper(mergedOutput, mergedOutput);
		
		Processor [] allOfThem = new Processor[5];
		allOfThem[0]= processKaggle;
		allOfThem[1]= normalize;
		allOfThem[2]= processImdb;
		allOfThem[3]= validate;
		allOfThem[4]= dedupe;
		processAll(allOfThem);
		}
	
	//method that calls execute on all elements of the Processor[]
	public static void processAll (Processor[] allProcessors) throws IOException {
		
		for( int i=0; i< allProcessors.length; i++) {
			allProcessors[i].execute();
		}

	}
}

