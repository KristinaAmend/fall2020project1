package movies.importer;
import java.util.ArrayList;
/**
 * A class that extends the Processor
 * Normalizer insures all movies have consistent formatting
 * @author Kristina Amend
 *
 */
public class Normalizer extends Processor{

	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	/** Method that takes as input non-standardized format of an ArrayList<String> and formats it
	 * @param kaggleInput An ArrayList<String> representing the output from KaggleImporter
	 * @return Returns an ArrayList<String> representing the results of each movie in a standard format
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> kaggleInput) {
		String pattern = "\\t(.*?)\\t";
		for(int i = 0; i<kaggleInput.size(); i++) {
			int startRuntimeString = kaggleInput.get(i).indexOf("\t")+1;
			int endRuntimeString = kaggleInput.get(i).indexOf("\t", kaggleInput.get(i).indexOf("\t") + 1);
			// retrieves the substring of minutes (which is after the 1st tab and before 2nd tab)
			String minutes = kaggleInput.get(i).substring(startRuntimeString,endRuntimeString);
			// substring of only the first string 
			minutes = minutes.substring(0,minutes.indexOf(" "));
			kaggleInput.set(i, kaggleInput.get(i).replaceFirst(pattern, "\t"+minutes+"\t"));
			kaggleInput.set(i, kaggleInput.get(i).toLowerCase());
		} 
		return kaggleInput;
	}

}