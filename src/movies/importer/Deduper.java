package movies.importer;
import java.util.ArrayList;

/**
 * @author Kausar Mussa
 * @author Kristina Amend 
 *This class is a Deduper that extends a Processor. The process method of this class 
 *will take as input a ArrayList of String of movies and will return an arrayList of Strings
 *that contains movies but with no duplicates. If ever two movies are the same the movies will be 
 *merged in one. To make the comparison of 2 movies easier we take the input and convert into an 
 *arrayList of Movies object in the helper method createMovies.
 *
 */

public class Deduper extends Processor{
	//constructor
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	/** Process extends the abstract Processor class to combine both ArrayList from the movie files
	 * into one result with no duplicate movies
	 * @param combined An ArrayList<String> representing the input from the processor
	 * @return Returns an ArrayList<String> representing the results of both movie files without duplicates
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> combined) {	
		ArrayList<Movie> movies = new ArrayList<Movie>();
		ArrayList<Movie> moviesNoDupli = new ArrayList<Movie>();
		ArrayList<String> noDuplicates = new ArrayList<String>();
		movies = createMovies(combined);
		
		for (int i=0; i< movies.size(); i++) {
			
			if(moviesNoDupli.contains(movies.get(i))) //What to do if the movie is already in the arrayList
			{
				int pos = moviesNoDupli.indexOf(movies.get(i));
				
				if(moviesNoDupli.get(pos).getSource().equals(movies.get(i).getSource())) {
					Movie mergedMovie = new Movie (movies.get(i).getName(),movies.get(i).getReleaseYear(),movies.get(i).getRuntime(),movies.get(i).getSource());
					moviesNoDupli.set(pos, mergedMovie); //replace the movie in the arrayList moviesNodpi with the new movie, same source
					}
				else {
					
					Movie mergedMovie = new Movie (movies.get(i).getName(),movies.get(i).getReleaseYear(),movies.get(i).getRuntime(),"kaggle;imdb");
					moviesNoDupli.set(pos, mergedMovie); //replace the movie in the arrayList moviesNodpi with the new movie and merged Source
				}
			}
						
			else {
				moviesNoDupli.add(movies.get(i)); //add new movie if it is not a duplicate
			}
		}
			//have to return an ArrayList of String
			for ( int k= 0; k<moviesNoDupli.size(); k++) {
				noDuplicates.add(moviesNoDupli.get(k).toString());
			}
		return noDuplicates;
	}
	/** method method that creates an ArrayList of Movies
	 * @param combined An ArrayList<String> representing the input of both movie files combined
	 * @return Returns an ArrayList<Movie> containing each movie from the input
	 */
	public static ArrayList<Movie> createMovies(ArrayList<String> combined) {
		final int title = 0;
		final int runtime = 1;
		final int year = 2;
		final int source = 3;
		ArrayList<Movie> movies = new ArrayList<Movie>();
		String split [] = new String[combined.size()];
		for (int i = 0; i < combined.size(); i++) {
			split = combined.get(i).split("\\t");
			Movie movieToAdd = new Movie(split[title], split[runtime], split[year], split[source]);
			movies.add(movieToAdd);
			}
		return movies;
	}
}