package movies.importer;
import java.io.IOException;
import java.lang.Math;

/**
 * Movie class contains information about each movie retrieved from raw data files
 * @author Kausar Mussa
 * @author Kristina Amend
 *
 */
public class Movie {

	private String name;
	private String runtime;
	private String releaseYear;
	private String source; // will be used to store where the Movie came from (kaggle or imdb)
	
	public Movie(String name, String runtime, String releaseYear, String source) {
		this.name = name;
		this.runtime = runtime;
		this.releaseYear = releaseYear;
		this.source = source;
	}

	// get methods
	public String getName() {
		return this.name;
	}

	public String getRuntime() {
		return this.runtime;
	}

	public String getReleaseYear() {
		return this.releaseYear;
	}

	public String getSource() {
		return this.source;
	}

	// Overrides toString to return the values of the 4 different fields separated by a tab
	public String toString() {
		return name + "\t" + runtime + "\t" + releaseYear + "\t" + source;
	}

	// Override equals to be able to compare two Movies
	@Override
	public boolean equals(Object o) {

		if (!(o instanceof Movie)) { // object is not a Movie
			return false;
		}
		try {
		int runtimeInt = Integer.parseInt(this.runtime);
		int runtimeToCompare = Integer.parseInt(((Movie) o).runtime);
		int diff = runtimeInt - runtimeToCompare;

		if (this.name.equals(((Movie) o).name) && this.releaseYear.equals(((Movie) o).releaseYear)
				&& Math.abs(diff) <= 5) {
			return true;
		}
		}
		catch (NumberFormatException e) {
		return false;

}
		return false;
	}
	/*public static void main(String[]args) throws IOException {
	Movie mov1 = new Movie ("titanic","315min","1997","kaggle");
	Movie mov2 = new Movie ("titanic","310","2004","imdb");
	System.out.println(mov1.equals(mov2));
	
}*/
}