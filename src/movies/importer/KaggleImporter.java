package movies.importer;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * A class that extends the Processor
 * KaggleImporter processes the results by overriding the process method
 * @author Kristina Amend
 *
 */
public class KaggleImporter extends Processor {

	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	/** Method that reads ArrayList<String> that came from the raw .txt file and produces a new
	 * ArrayList<String> where each element represents a movie
	 * @param kaggleInput An ArrayList<String> representing the output from KaggleImporter
	 * @return Returns an ArrayList<String> representing the results of each movie in a standard format
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> kaggleInput) {
		final int titleColumn = 15;
		final int runtimeColumn = 13;
		final int yearColumn = 20;
		String split [] = new String[kaggleInput.size()];

		for (int i = 0; i < kaggleInput.size(); i++) {
			split = kaggleInput.get(i).split("\\t");

			for (int j = 0; j < kaggleInput.size(); j++) {
				if (split.length == 21) {
					Movie addMovie = new Movie(split[titleColumn], split[runtimeColumn], split[yearColumn], "kaggle");
					kaggleInput.set(i, addMovie.toString());
				}
				else {
					break;
				}
			}
		}
		return kaggleInput;
	}
}