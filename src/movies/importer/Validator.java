package movies.importer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Kausar Mussa
 * This class Validator extends Processor. The overridden process method 
 * will take as input the imported values of the imdbimporter
 * and return an arrayList that has no null value or any empty string. 
 * It will also check that the runtime and release year can be converted
 * to an int. If any movie does not qualify for any of these requirements
 * it will not be added to the returned ArrayList
 *
 */

public class Validator extends Processor {

	//constructor
	public Validator(String sourceDir, String outputDir, boolean header) 
	{
		super(sourceDir, outputDir, false);
	}

	public ArrayList<String> process (ArrayList<String> input) 
	{
		ArrayList <String> inputLower = changeCase(input);
		ArrayList <String> validated = new ArrayList <String>();

		String each = "";
		String [] checkValues;

		int runtime = 1;
		int year = 2;

		for ( int i = 0; i< inputLower.size(); i++) {
			each = inputLower.get(i);
			checkValues= each.split("\\t",-1);

			for( int j=0; j<checkValues.length; j++) {
				if(checkValues[j]==(null) || checkValues[j].length()== 0 ) //checking if any null values or empty string
				{
					break;		//if null or empty the loop will break as we do not want that movie in our returned arrayList 	
				}
				else if (j == (checkValues.length-1)) //once we checked all element of my String array we must check the 2nd requirement
				{ 

					try {
						Integer.parseInt(checkValues[runtime]);
						Integer.parseInt(checkValues[year]);
						validated.add(each); //if no exception is throws meaning the runtime and year can be converted to an int then the movie will be added to the list. 
					}
					catch (NumberFormatException e ) {// if exception caught then nothing is added to validated.
					}
				}
			}	
		}
		return validated;	}
	
	//helper method to return an arraylist of only lowercase letters
		public ArrayList <String> changeCase ( ArrayList <String> toChange) 
		{
			ArrayList <String> asLower = new ArrayList <String>();
			String toAdd;
			for ( int i = 0; i< toChange.size() ; i++) 
			{
				toAdd = toChange.get(i).toLowerCase();
				asLower.add(toAdd);
			}
			return asLower;
		}


}

