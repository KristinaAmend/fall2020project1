package movies.tests;
import movies.importer.Movie;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Tests methods found in the Movie class
 * @author Kausar Mussa
 * @author Kristina Amend 
 *
 */

class Moviestest {

	@Test
	void testGetMethods() {
		Movie alpha = new Movie ("Mom's Life","180","1998","imdb");
		Movie beta = new Movie ("Dad's Life","221","2000","kaggle");
		
		//testing the get methods with 2 different movie objects
		assertEquals("Dad's Life", beta.getName());
		assertEquals("221", beta.getRuntime());
		assertEquals("1998", alpha.getReleaseYear());
		assertEquals("imdb", alpha.getSource());
		
	}

	@Test
	void testToStringMethod() {
		Movie mov1 = new Movie ("Titanic","314","1997","kaggle");
		Movie mov2 = new Movie ("Mean Girls","97","2004","imdb");
		assertEquals(mov1.toString(),"Titanic	314	1997	kaggle");
		assertEquals(mov2.toString(),"Mean Girls	97	2004	imdb");
	}
	@Test
	void testEqualsMethod() {
		Movie mov1 = new Movie ("titanic","315","1997","kaggle");
		Movie mov2 = new Movie ("titanic","310","2004","imdb");
		Movie mov3 = new Movie ("titanic","310","1997","kaggle");
		Movie mov4 = new Movie ("titanic","319","1997","imdb");
		Movie mov5 = new Movie ("mean girls","192","2004","imdb");
		Movie mov6 = new Movie ("girls","192","2004","imdb");
		Movie mov7 = new Movie ("mean girls","192","2004","kaggle");
		Object obj = new Object();
		assertNotEquals(mov1, obj); // not equal -> true
		assertNotEquals(mov1, mov2); // not equal -> true
		assertNotEquals(mov5, mov6); // not equal -> true
		assertEquals(mov1, mov3); // equal -> true
		assertEquals(mov1, mov4); // equal -> true
		assertEquals(mov5, mov7); // equal -> true
	}

}