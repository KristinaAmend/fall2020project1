package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;
/**
 * Tests the expected output ArrayList<String> when KaggleImporter is processed
 * @author Kristina Amend
 *
 */
class KaggleImporterTest {

	@Test
	public void testKaggle() {
		ArrayList<String> testInput = new ArrayList<String>();
		String test0 = "Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016";
		String expected0 = "The Masked Saint	111 minutes	2016	kaggle";
		
		String test1 = "Jim Carter	Rasmus Hardiker	Alice Krige	Tim Pigott-Smith	Miriam Margolyes	Matthew Marsh	\"THE LITTLE VAMPIRE, based on the characters from the bestselling novels by Angela Sommer-Bodenburg, tells the story of Rudolph, a thirteen year old vampire, whose clan is threatened by a notorious vampire hunter. He meets Tony, a mortal of the same age, who is fascinated by old castles, graveyards and - vampires. Tony helps Rudolph in an action and humor packed battle against their adversaries, together they save Rudolph's family and become friends.\"	Richard Claus	Karsten Kiilerich	Director Not Available	Action	NR	8/28/2018	82 minutes	Studio Not Available	The Little Vampire 3D	Angela Sommer-Bodenburg	Larry Wilson	Richard Claus	Writer Not Available	2017";
		String expected1 = "The Little Vampire 3D	82 minutes	2017	kaggle";
		
		String test2 = "Robert Mitchum	Deborah Kerr	Cast Not Available	Cast Not Available	Cast Not Available	Cast Not Available	\"A two-person character study directed by John Huston, Heaven Knows Mr. Allison stars Robert Mitchum as a World War II Marine sergeant and Deborah Kerr as a Roman Catholic nun. Both nun and sergeant are marooned on a South Pacific island, hemmed in by surrounding Japanese troops. Mitchum does his best to make the nun's ordeal less painful, but is torn by his growing love for her. Kerr is equally fond of Mitchum, but refuses to renounce her vows. Their unrealized ardor mellows into mutual respect as they struggle to survive before help arrives. Based on a novel by Charles K. Shaw, Heaven Knows, Mr. Allison was coproduced by Eugene Frenke, who later filmed a low-budget variation on the story, The Nun and the Sergeant (62), which starred Frenke's wife Anna Sten. ~ Hal Erickson, Rovi\"	John Huston	Director Not Available	Director Not Available	Action	NR	1/1/1957	108 minutes	Fox	Heaven Knows Mr. Allison	John Lee Mahin	John Huston	Writer Not Available	Writer Not Available	1957";
		String expected2 = "Heaven Knows Mr. Allison	108 minutes	1957	kaggle";
		
		testInput.add(0, test0);
		testInput.add(1, test1);
		testInput.add(2, test2);
		
		KaggleImporter kaggleTest = new KaggleImporter("", "");
		testInput = kaggleTest.process(testInput);
		
		assertEquals(expected0, testInput.get(0));
		assertEquals(expected1, testInput.get(1));
		assertEquals(expected2, testInput.get(2));
	}

}