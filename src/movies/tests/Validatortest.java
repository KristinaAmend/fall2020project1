package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;


import org.junit.jupiter.api.Test;

import movies.importer.Validator;

class Validatortest {

	@Test
	void testValidator() {
		ArrayList <String> inputTest = new ArrayList <String>();
		String test0 = "miss jerry	45	1894	imdb";
		String expected0 = "miss jerry	45	1894	imdb";
		
		String test1 = "the story of the kelly gang	70	1906	imdb";
		String expected1 = "the story of the kelly gang	70	1906	imdb";
		
		
		String test2 = "the fortune	90min	1975	imdb";
		
		String test3 = "the fortune	90	1975years	imdb";
		
		inputTest.add(0,test0);
		inputTest.add(1,test1);
		inputTest.add(2, test2);
		inputTest.add(3, test3);
		
		Validator imdbTest = new Validator ("","",false);
		
		inputTest = imdbTest.process(inputTest);
		
		assertEquals(expected0, inputTest.get(0)); //test0 is valid to be a movie so will be in the ArrayList -->true
		assertEquals(expected1, inputTest.get(1));//test1 is valid to be a movie so will be in the ArrayList-->true
		assertEquals(2, inputTest.size()); //because test2 and test3 have a runtime/releaseYear that cannot be converted to an int it will not be in the ArrayList so the size will be 2 -- > true

		
	}

}
