package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;
/**
 * Tests the expected output ArrayList<String> when Normalizer is processed
 * @author Kristina Amend
 *
 */
class NormalizerTest {

	@Test
	public void testNormalizer() {
		ArrayList<String> testInput = new ArrayList<String>();
		String test0 = "The Masked Saint	minutes 444	2016	kaggle";
		String expected0 = "the masked saint	minutes	2016	kaggle";
		
		String test1 = "The Little Vampire 3D	82 minutes	2017	kaggle";
		String expected1 = "the little vampire 3d	82	2017	kaggle";
		
		String test2 = "Heaven Knows Mr. Allison	108 108	1957	kaggle";
	    String expected2 = "heaven knows mr. allison	108	1957	kaggle";
		
		testInput.add(0, test0);
		testInput.add(1, test1);
		testInput.add(2, test2);
		
		Normalizer normalizeTest = new Normalizer("", "");
		testInput = normalizeTest.process(testInput);
		
		assertEquals(expected0, testInput.get(0));
		assertEquals(expected1, testInput.get(1));
		assertEquals(expected2, testInput.get(2));
	}

}