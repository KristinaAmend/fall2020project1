package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;

class ImdbImportertest {

	@Test
	void testImdb() {
		
		ArrayList <String> inputTest = new ArrayList <String>();
		String test0 = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2\r\n"; 
		String expected0 = "miss jerry	45	1894	imdb";
		
		String test1 = "\"tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\\\"Biography, Crime, Drama\\\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\\\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\\\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\\\"$2,250 \\\"				7	7\";";
		String expected1 = "the story of the kelly gang	70	1906	imdb";
		
		String test2 = "tt0002461	The Fortune	The Fortune	1975	10/15/1975	Drama	90	\"France, USA\"	English	\"Andr� Calmettes, James Keane\"	\"James Keane, William Shakespeare\"	Le Film d'Art	\"Robert Gemp, Frederick Warde, Albert Gardner, James Keane, George Moss, Howard Stuart, Virginia Rankin, Violet Stuart, Carey Lee, Carlotta De Felice\"	This title has been doctored to make it so it matches with an entry in the other file.	5.5	225	\"$30,000 \"				8	1";
		String expected2 = "the fortune	90	1975	imdb";
		
		inputTest.add(0,test0);
		inputTest.add(1,test1);
		inputTest.add(2, test2);
		
		ImdbImporter imdbTest = new ImdbImporter ("","",true);
		
		inputTest = imdbTest.process(inputTest);
		
		assertEquals(expected0, inputTest.get(0));
		assertEquals(expected1, inputTest.get(1));
		assertEquals(expected2, inputTest.get(2));
		
	}

}
